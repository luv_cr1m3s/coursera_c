#include <stdio.h>

int main(void){
	
	printf("\t\t\tNow you want me to tell you\n\
			a history of fish\n\
			while the lake clouds over?\n\
			But don’t you see\n\
			how thirst beats in the throat\n\
			of the lizards on the crushed leaves?\n\
			On the ground\n\
			autumn’s dead hedgehogs\n\
			have plunged through the periwinkles.\n\
			And you chew\n\
			the parched stalks:\n\
			already the corner of your lip is bleeding a bit.\n\
			And now\n\
			you want me to tell you\n\
			the history of birds?\n\
			But in the heat\n\
			of noon, the wild cuckoo\n\
			flies alone.\n\
			And still\n\
			the lost puppy howls among the brambles:\n\
			perhaps the bay horse, running,\n\
			struck him with a black hoof\n\
			on his snout.\n");

	return 0;
}
