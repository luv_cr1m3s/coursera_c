#include <stdio.h>
#include <math.h>

int main(void){

  double user_input = 0;

  printf("Enter value between 0 and 1 (non inclusive) to get sin value of it: ");
  scanf("%lf", &user_input);

  if( user_input <= 0 || user_input >= 1){
    printf("Plesse input value between 0 and 1 non inclusive\n");
    return -1;
  }

  printf("sin of %.3lf is : %.3lf\n", user_input, sin(user_input));

  return 0;
}

