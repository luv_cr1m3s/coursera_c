#include <stdio.h>
#include <stdlib.h>

// this program reads data from 'data.txt' file
// and calculates average

int main(void){
	// at first try to open file 'data.txt'
	FILE *data = fopen("data.txt", "r");
	//stop execution and return -1 if file canot be read
	if (data == NULL){
		printf("Can't read file 'data.txt'\n");
		exit(-1);
	}
	// counter used to count number of numbers present in the file
	// tmp just value used a result of fscanf
	int counter = 0;
	int tmp = 0;
	// read data till meet '\0'
	while(fscanf(data, "%d", &tmp) != EOF){
		counter++;
	}
	// return file pointer to the beginning
	rewind(data);
	// malloc enough memory for array
	int * data_array = (int *)malloc(counter * sizeof(int));
	int i = 0;
	// write numbers from the file into array
	while(fscanf(data, "%d", &data_array[i]) != EOF){
		i++;
	}
	// close file
	fclose(data);
	// used max int size data type to prevent overflow
	long long int sum = 0;
	// calculate sum
	for(int j = 0; j < counter; j++){
		sum += data_array[j];
	}
	// don't forget to free memory
	free(data_array);
	// prints average
	printf("Average: %lld\n", sum / counter);

	return 0;
}
