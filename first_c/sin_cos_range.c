#include <stdio.h>
#include <math.h>

// change constant to reduce/increase range of values
// keep it float to get correct value of step
#define RANGE 10.0

int main(void){

	double step = 0;
	
	printf("|%-5s|%-5s|%-5s|\n", "STEP", "SIN", "COS"); 
	printf("%19s\n", "-------------------");
	
	for(int i = 0; i <= (int)RANGE; i++){
		
		step = i / RANGE;
		printf("|%.3lf|%.3lf|%.3lf|\n", step, sin(step), cos(step));
	}

	printf("%19s\n", "-------------------");

	return 0;
}
